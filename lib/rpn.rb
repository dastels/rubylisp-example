require_relative './stack'
require 'rubylisp'

class Rpn

  def initialize
    @stack = Stack.new
  end
  

  def numeric?(token)
    result = true
    token.each_char do |ch|
      return false if ch < "0"
      return false if ch > "9"
    end
    true
  end


  def operator?(token)
    "+-*/".include?(token)
  end


  def function_name?(token)
    f = Lisp::EnvironmentFrame.global.value_of(Lisp::Symbol.named(token))
    return false if f.nil?
    f.function?
  end


  def evaluate_operator(token)
    case token
    when "+"
      @stack.push(@stack.pop + @stack.pop)
    when "-"
      b = @stack.pop
      @stack.push(@stack.pop - b)
    when "*"
      @stack.push(@stack.pop * @stack.pop)
    when "/"
      b = @stack.pop
      @stack.push(@stack.pop / b)      
    else
      raise :invalid_operator
    end
  end


  def evaluate_function(function_name)
    f = Lisp::EnvironmentFrame.global.value_of(Lisp::Symbol.named(function_name))
    args= []
    f.arity.times {|i| args.unshift(Lisp::Number.with_value(@stack.pop))}
    result = f.apply_to(Lisp::ConsCell.array_to_list(args), Lisp::EnvironmentFrame.global)
    @stack.push(result.value)
  end
  

  def eval(token)
    if numeric?(token)
      @stack.push(token.to_i)
    elsif operator?(token)
      evaluate_operator(token)
    elsif function_name?(token)
      evaluate_function(token)
    else
      error
    end
  end


  def evaluate(expression)
    expression.split.each {|token| eval(token) }
    @stack.pop
  end
end

Lisp::Initializer.initialize_global_environment
Lisp::Initializer.register_builtins

Lisp::Primitive.register("ruby-square", "", true) { |args, env| ruby_square_impl(args, env) }

def ruby_square_impl(args, env)
  raise "ruby_square needs 1 argument, but received #{args.length}" if args.length != 1
  arg = args.car.evaluate(env)
  raise "ruby_square needs a numeric argument, but received #{arg}" unless arg.number?
  Number.with_value(arg.value * arg.value)
end


code = File.open("./functions.lsp") {|f| f.read }
Lisp::Parser.new.parse_and_eval_all(code)
calc = Rpn.new
puts calc.evaluate(ARGV[0])
